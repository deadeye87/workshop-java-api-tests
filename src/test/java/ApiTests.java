import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class ApiTests {

    @Test
    void testApiGetRequest(){

        Response response = get("http://localhost:3000/posts/1");

        System.out.println("Response = "+ response.asString());
        System.out.println("Status code = "+ response.getStatusCode());
        System.out.println("Body ="+ response.getBody().asString());
        System.out.println("Time taken ="+ response.getTime());
        System.out.println("Header ="+ response.getHeader("content-type"));

    }

    @Test
    void testGetRequestSendStatusCode200(){
        given().
            get("http://localhost:3000/posts/1").
        then().
            statusCode(200);

    }

    @Test
    void testPostRequestSendStatusCode201(){
        Map<String, String> map = new HashMap<String, String>();

        map.put("title", "json-server-test-post");
        map.put("author","Tester1");
        map.put("body", "Post some comment");
        map.put("postId", "1");

        System.out.println(map);

        JSONObject request = new JSONObject();

        System.out.println(request.toJSONString());

        baseURI = "http://localhost:3000";

        given().
            header("Content-Type", "application/json").
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                body(request.toJSONString()).
        when().
            post("/posts").
        then().
            statusCode(201);
    }

    @Test
    void testPutRequestSendStatusCode200(){

        JSONObject request = new JSONObject();

        System.out.println(request.toJSONString());

        baseURI = "http://localhost:3000";

        given().
            header("Content-Type", "application/json").
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                body(request.toJSONString()).
        when().
            post("/posts/2").
        then().
            statusCode(200);

    }
}
